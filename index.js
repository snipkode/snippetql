const fs    = require('fs');
const path  = require('path');

const generateRandomString = (length = 10) => {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
};

class SnippetQL {
    static uri = '';
    static collectionPath = '';
    static selectFields = [];
    static whereClauses = [];
    static updateFields = {};
    static limitValue = null;
    static offsetValue = null;
    static orderByField = null;
    static orderByDirection = 'asc';
    static hiddenFields = [];

    static initializeDB(uri = './database/db.spl') {
        try {
            this.uri = uri;
            const dirPath = path.dirname(uri);
            fs.mkdirSync(dirPath, { recursive: true });
            const exists = fs.existsSync(uri);
            if (!exists) {
                fs.writeFileSync(uri, JSON.stringify({}, null, 2), 'utf8');
            }
        } catch (error) {
            console.error("Error initializing database:", error);
        }
    }

    static collection(path) {
        this.collectionPath = path;
        return this;
    }

    static select(fields) {
        if (fields === '*') {
            this.selectFields = fields;
        } else {
            this.selectFields = fields.split(',').map(field => field.trim());
        }
        return this;
    }

    static hideField(field) {
        this.hiddenFields.push(field);
        return this;
    }

    static where(field) {
        this.whereClauses.push({ field, operator: '==', value: null, logic: 'AND' });
        return this;
    }

    static equal(value) {
        const lastClause = this.whereClauses[this.whereClauses.length - 1];
        lastClause.operator = '==';
        lastClause.value = value;
        return this;
    }

    static notEqual(value) {
        const lastClause = this.whereClauses[this.whereClauses.length - 1];
        lastClause.operator = '!=';
        lastClause.value = value;
        return this;
    }

    static and(field) {
        this.whereClauses.push({ field, operator: '==', value: null, logic: 'AND' });
        return this;
    }

    static or(field) {
        this.whereClauses.push({ field, operator: '==', value: null, logic: 'OR' });
        return this;
    }

    static limit(value) {
        this.limitValue = value;
        return this;
    }

    static offset(value) {
        this.offsetValue = value;
        return this;
    }

    static orderBy(field, direction = 'asc') {
        this.orderByField = field;
        this.orderByDirection = direction.toLowerCase();
        return this;
    }

    static insert(newData) {
        try {
            const data = this.readDataFromFile();
            if (!data[this.collectionPath]) {
                data[this.collectionPath] = {};
            }
            const newId = generateRandomString();
            data[this.collectionPath][newId] = newData;
            this.writeDataToFile(data);
            return true;
        } catch (error) {
            console.error("Error inserting data:", error);
            return false;
        }
    }


    static update(newData) {
        try {
            const data = this.readDataFromFile();
            const whereClausesCopy = [...this.whereClauses]; // Salin klausa where ke variabel lokal
            const collection = data[this.collectionPath];
            let updated = false;
            for (let key in collection) {
                if (this.evaluateWhereClauses(collection[key], whereClausesCopy)) {
                    Object.assign(collection[key], newData);
                    updated = true;
                }
            }
            if (updated) {
                this.writeDataToFile(data);
                return true;
            } else {
                console.log("No data matching the specified criteria found for update.");
                return false;
            }
        } catch (error) {
            console.error("Error updating data:", error);
            return false;
        }
    }
    
    
    

    static delete() {
        try {
            const data = this.readDataFromFile();
            const whereClauses = [...this.whereClauses]; // Copy whereClauses to local variable
            if (whereClauses.length > 0) {
                const collection = data[this.collectionPath];
                let deleted = false;
                for (let key in collection) {
                    if (this.evaluateWhereClauses(collection[key], whereClauses)) {
                        delete collection[key];
                        deleted = true;
                    }
                }
                if (deleted) {
                    this.writeDataToFile(data);
                    return true;
                } else {
                    console.log("No data matching the specified criteria found for deletion.");
                    return false;
                }
            } else {
                console.log("No where clause provided for deletion.");
                return false;
            }
        } catch (error) {
            console.error("Error deleting data:", error);
            return false;
        }
    }    
    
    

    static readDataFromFile() {
        try {
            const data = fs.readFileSync(this.uri, 'utf-8');
            return JSON.parse(data);
        } catch (error) {
            console.error("Error reading data from file:", error);
            return {};
        }
    }

    static writeDataToFile(data) {
        try {
            fs.writeFileSync(this.uri, JSON.stringify(data, null, 2), 'utf-8');
            return true;
        } catch (error) {
            console.error("Error writing data to file:", error);
            return false;
        }
    }

    static evaluateWhereClauses(item) {
        let matches = true;
        if (this.whereClauses.length === 0) {
            return true;
        }
        for (const clause of this.whereClauses) {
            const itemValue = item[clause.field];
            let clauseMatches = false;
            switch (clause.operator) {
                case '==':
                    clauseMatches = itemValue === clause.value;
                    break;
                case '!=':
                    clauseMatches = itemValue !== clause.value;
                    break;
                default:
                    clauseMatches = false;
            }
            if (clause.logic === 'AND') {
                matches = matches && clauseMatches;
            } else if (clause.logic === 'OR') {
                matches = matches || clauseMatches;
            } else {
                matches = clauseMatches;
            }
        }
        return matches;
    }
    

    static build() {
        const data = this.readDataFromFile();
        const whereClauses = [...this.whereClauses]; // Copy whereClauses to local variable
        const selectFields = this.selectFields;
        const orderByField = this.orderByField;
        const orderByDirection = this.orderByDirection;
        const limitValue = this.limitValue;
        const offsetValue = this.offsetValue;
    
        let result = Object.values(data[this.collectionPath] || []);
    
        if (whereClauses.length > 0) {
            result = result.filter(item => this.evaluateWhereClauses(item, whereClauses));
        }
    
        if (selectFields === '*') {
            result = result.map(item => {
                const selectedItem = {};
                for (const field in item) {
                    if (!this.hiddenFields.includes(field)) {
                        selectedItem[field] = item[field];
                    }
                }
                return selectedItem;
            });
        } else if (selectFields.length > 0) {
            result = result.map(item => {
                const selectedItem = {};
                selectFields.forEach(field => {
                    if (!this.hiddenFields.includes(field) && item.hasOwnProperty(field)) {
                        selectedItem[field] = item[field];
                    }
                });
                return selectedItem;
            });
        }
    
        if (orderByField !== null) {
            result.sort((a, b) => {
                const aValue = a[orderByField];
                const bValue = b[orderByField];
                if (aValue < bValue) return orderByDirection === 'asc' ? -1 : 1;
                if (aValue > bValue) return orderByDirection === 'asc' ? 1 : -1;
                return 0;
            });
        }
    
        if (offsetValue !== null) {
            result = result.slice(offsetValue);
        }
    
        if (limitValue !== null) {
            result = result.slice(0, limitValue);
        }
    
        return result;
    }
    
}

module.exports = SnippetQL;
