## QUICK START CRUD

- Initialize Database

```javascript

    const SnippetQL = require('snippetql');

    SnippetQL.initializeDB();
    const usersCollection = SnippetQL.collection("users");

```

- Insert Database

```javascript

    usersCollection.insert({ username: "alamwibowo", password: "123456" });

```


- Update Database

```javascript
  /**
   * Fungsi Update Data
   * @return {Boolean}
   */
    const update = usersCollection
                    .where("username")
                    .equal("alamwibowo")
                    .update({ address: "Jl. Alam Selangit 2" });

```


- Delete Database

```javascript
  /**
   * Fungsi Delete Data
   * @return {Boolean}
   */
  const hapus = usersCollection.where("username").equal("alamwibowo").delete();

```

- Select Database

```javascript

    /**
   * Fungsi query dengan where clauses
   * @return {Object}
   */
    const getAll = usersCollection
      .select("*")
      .hideField("password")
      .build();

  /**
   * Fungsi query dengan where clauses
   * @return {Object}
   */
  const getUsernameAndPassword = usersCollection
    .where("username")
    .equal("alamwibowo")
    .and("password")
    .equal("123456")
    .build();

```